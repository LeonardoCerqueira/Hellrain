// Fill out your copyright notice in the Description page of Project Settings.


#include "Item.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Particles/ParticleSystem.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"

// Sets default values
AItem::AItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ColliderVolume = CreateDefaultSubobject<USphereComponent>(TEXT("ColliderVolume"));
	SetRootComponent(ColliderVolume);

	ColliderVolume->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	ColliderVolume->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
	ColliderVolume->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	ColliderVolume->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);

	ColliderVolume->SetSphereRadius(64.f);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(GetRootComponent());

	IdleParticles = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("IdleParticles"));
	IdleParticles->SetupAttachment(GetRootComponent());

	bRotating = false;
	RotationRate = 0.f;

	bFloating = false;
	FloatSpeed = 0.f;
	FloatRange = 0.f;
}

// Called when the game starts or when spawned
void AItem::BeginPlay()
{
	Super::BeginPlay();

	ColliderVolume->OnComponentBeginOverlap.AddDynamic(this, &AItem::OnOverlapBegin);
	ColliderVolume->OnComponentEndOverlap.AddDynamic(this, &AItem::OnOverlapEnd);

	InitialLocation = GetActorLocation();
	bGoingUp = true;
}

// Called every frame
void AItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bRotating)
	{
		FRotator NewRotation = GetActorRotation();
		NewRotation.Yaw += RotationRate * DeltaTime;
		SetActorRotation(NewRotation);
	}

	if (bFloating)
	{
		float FloatDirection = (bGoingUp ? 1.f : -1.f);
		FVector NewLocation = GetActorLocation();
		
		NewLocation.Z += FloatSpeed * DeltaTime * FloatDirection;
		if (FMath::Abs<float>(NewLocation.Z - InitialLocation.Z) >= FloatRange)
		{
			bGoingUp = !bGoingUp;
		}
		SetActorLocation(NewLocation);
	}
}

void AItem::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("AItem::OnOverlapBegin()"));

	if (OverlapParticles)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), OverlapParticles, GetActorLocation(), FRotator::ZeroRotator, true);
	}

	if (SoundCue)
	{
		UGameplayStatics::PlaySound2D(GetWorld(), SoundCue);
	}
}

void AItem::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	UE_LOG(LogTemp, Warning, TEXT("AItem::OnOverlapEnd()"));
}
