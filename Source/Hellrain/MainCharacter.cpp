// Fill out your copyright notice in the Description page of Project Settings.


#include "MainCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/Controller.h"
#include "Camera/CameraComponent.h"
#include "Engine/World.h"
#include "Components/InputComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Weapon.h"

// Sets default values
AMainCharacter::AMainCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create Camera Boom (pulls towards the player if there's a collision) 
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(GetRootComponent());
	CameraBoom->TargetArmLength = 600.f; // Camera follows at this distance
	CameraBoom->bUsePawnControlRotation = true; // Rotate arm based on controller

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(37.f, 102.f);

	// Create Follow Camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	// Attach the camera to the end of the boom and let it adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false;

	// Set our turn rates for input
	BaseTurnRate = 65.f;
	BaseLookUpRate = 65.f;

	// Don't rotate when the controller rotates,
	// let that just affect the camera
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationPitch = false;

	// Configure character movement
	// Character moves in the direction  of input...
	GetCharacterMovement()->bOrientRotationToMovement = true;
	// ...at this rotation rate
	GetCharacterMovement()->RotationRate = FRotator(0.f, 600.f, 0.f);
	GetCharacterMovement()->JumpZVelocity = 450.f;
	GetCharacterMovement()->AirControl = 0.1f;

	// Initialize movement variables
	NormalMaxSpeed = 650.f;
	SprintingMaxSpeed = 950.f;
	SetMovementStatus(EMovementStatus::EMS_Normal);
	bSprintKeyDown = false;

	StaminaDrainRate = 60.f;
	MinSprintStamina = 120.f;
	SetStaminaStatus(EStaminaStatus::ESS_Normal);

	// Initialize player stats
	MaxHealth = 100.f;
	Health = MaxHealth;
	MaxStamina = 400.f;
	Stamina = MaxStamina;
	Coins = 0;
}

// Called when the game starts or when spawned
void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMainCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ManageStamina(DeltaTime);
}

// Called to bind functionality to input
void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Pressed, this, &AMainCharacter::StartSprint);
	PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Released, this, &AMainCharacter::EndSprint);

	PlayerInputComponent->BindAction(TEXT("Interact"), IE_Pressed, this, &AMainCharacter::StartInteraction);
	PlayerInputComponent->BindAction(TEXT("Interact"), IE_Released, this, &AMainCharacter::EndInteraction);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AMainCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AMainCharacter::MoveRight);
	
	PlayerInputComponent->BindAxis(TEXT("Turn"), this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(TEXT("TurnRate"), this, &AMainCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis(TEXT("LookUpRate"), this, &AMainCharacter::LookUpAtRate);
}

void AMainCharacter::SetMovementStatus(EMovementStatus Status)
{
	MovementStatus = Status;
	float MaxWalkSpeed = (MovementStatus == EMovementStatus::EMS_Sprinting ? SprintingMaxSpeed : NormalMaxSpeed);
	GetCharacterMovement()->MaxWalkSpeed = MaxWalkSpeed;
}

void AMainCharacter::MoveForward(float Value) 
{
	if (Controller != nullptr && !FMath::IsNearlyZero(Value))
	{
		// Find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0.f, Rotation.Yaw, 0.f);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}


void AMainCharacter::MoveRight(float Value)
{
	if (Controller != nullptr && !FMath::IsNearlyZero(Value))
	{
		// Find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0.f, Rotation.Yaw, 0.f);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Direction, Value);
	}
}

void AMainCharacter::TurnAtRate(float Rate)
{
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AMainCharacter::LookUpAtRate(float Rate)
{
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AMainCharacter::StartSprint()
{
	bSprintKeyDown = true;
}

void AMainCharacter::EndSprint()
{
	bSprintKeyDown = false;
}

void AMainCharacter::IncrementHealth(float Amount)
{
	Health += Amount;
}

void AMainCharacter::DecrementHealth(float Amount)
{
	Health -= Amount;

	if (Health <= 0)
	{
		Die();
	}
}

void AMainCharacter::IncrementStamina(float Amount)
{
	Stamina += Amount;
	if (Stamina >= MaxStamina)
	{
		Stamina = MaxStamina;
	}
}

void AMainCharacter::DecrementStamina(float Amount)
{
	Stamina -= Amount;
	if (Stamina <= 0.f)
	{
		Stamina = 0.f;
	}
}

void AMainCharacter::IncrementCoins(int32 Amount)
{
	Coins += Amount;
}

void AMainCharacter::DecrementCoins(int32 Amount)
{
	Coins -= Amount;
}

void AMainCharacter::Die()
{
	UE_LOG(LogTemp, Warning, TEXT("THE CHARACTER DIED"));
}

void AMainCharacter::ManageStamina(float DeltaTime)
{
	FVector2D HorizontalVelocity = FVector2D(GetVelocity().X, GetVelocity().Y);
	bool bIsCharacterMoving = !HorizontalVelocity.IsNearlyZero();

	float DeltaStamina = StaminaDrainRate * DeltaTime;

	switch (StaminaStatus)
	{
	case EStaminaStatus::ESS_Normal:
		if (bSprintKeyDown && bIsCharacterMoving)
		{
			DecrementStamina(DeltaStamina);
			if (Stamina < MinSprintStamina)
			{
				SetStaminaStatus(EStaminaStatus::ESS_BelowMinimum);
			}
			SetMovementStatus(EMovementStatus::EMS_Sprinting);
		}
		else
		{
			IncrementStamina(DeltaStamina);
			SetMovementStatus(EMovementStatus::EMS_Normal);
		}
		break;

	case EStaminaStatus::ESS_BelowMinimum:
		if (bSprintKeyDown && bIsCharacterMoving)
		{
			DecrementStamina(DeltaStamina);
			if (Stamina <= 0.f)
			{
				SetStaminaStatus(EStaminaStatus::ESS_Exhausted);
			}
			SetMovementStatus(EMovementStatus::EMS_Sprinting);
		}
		else
		{
			IncrementStamina(DeltaStamina);
			if (Stamina >= MinSprintStamina)
			{
				SetStaminaStatus(EStaminaStatus::ESS_Normal);
			}
			SetMovementStatus(EMovementStatus::EMS_Normal);
		}
		break;

	case EStaminaStatus::ESS_Exhausted:
		if (bSprintKeyDown && bIsCharacterMoving)
		{
			SetMovementStatus(EMovementStatus::EMS_Normal);
		}
		else
		{
			IncrementStamina(DeltaStamina);
			SetStaminaStatus(EStaminaStatus::ESS_ExhaustedRecovering);
			SetMovementStatus(EMovementStatus::EMS_Normal);
		}
		break;

	case EStaminaStatus::ESS_ExhaustedRecovering:
		IncrementStamina(DeltaStamina);
		if (Stamina >= MinSprintStamina)
		{
			SetStaminaStatus(EStaminaStatus::ESS_Normal);
		}
		SetMovementStatus(EMovementStatus::EMS_Normal);
		
		break;
	
	default:
		break;
	}
}

void AMainCharacter::StartInteraction()
{
	bInteractKeyDown = true;

	if (!ActiveOverlappingItem) { return; }

	AWeapon* Weapon = Cast<AWeapon>(ActiveOverlappingItem);
	if (Weapon)
	{
		Weapon->Equip(this);
		SetActiveOverlappingItem(nullptr);
	}
}

void AMainCharacter::EndInteraction()
{
	bInteractKeyDown = false;
}

void AMainCharacter::SetWeaponEquipped(AWeapon* Weapon)
{
	if (WeaponEquipped)
	{
		WeaponEquipped->Destroy();
	}

	WeaponEquipped = Weapon;
}