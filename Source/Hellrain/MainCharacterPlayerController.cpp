// Fill out your copyright notice in the Description page of Project Settings.


#include "MainCharacterPlayerController.h"
#include "Blueprint/UserWidget.h"

void AMainCharacterPlayerController::BeginPlay()
{
    Super::BeginPlay();

    if (HUDOverlayAsset)
    {
        HUDOverlay = CreateWidget<UUserWidget>(this, HUDOverlayAsset);
    }

    if (HUDOverlay)
    {
        HUDOverlay->AddToViewport();
        HUDOverlay->SetVisibility(ESlateVisibility::Visible);
    }
}