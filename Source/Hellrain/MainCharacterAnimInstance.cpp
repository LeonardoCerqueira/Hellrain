// Fill out your copyright notice in the Description page of Project Settings.


#include "MainCharacterAnimInstance.h"
#include "GameFramework/PawnMovementComponent.h"

void UMainCharacterAnimInstance::NativeInitializeAnimation()
{
    if (Pawn == nullptr)
    {
        Pawn = TryGetPawnOwner();
    }
}

void UMainCharacterAnimInstance::UpdateAnimationProperties()
{
    if (Pawn == nullptr)
    {
        Pawn = TryGetPawnOwner();
    }

    if (Pawn)
    {
        FVector Speed = Pawn->GetVelocity();
        FVector HorizontalSpeed = FVector(Speed.X, Speed.Y, 0.f);
        MovementSpeed = HorizontalSpeed.Size();

        bIsInAir = Pawn->GetMovementComponent()->IsFalling();
    }
}