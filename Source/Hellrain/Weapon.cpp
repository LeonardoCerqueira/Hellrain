// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "MainCharacter.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"

AWeapon::AWeapon()
{
    Mesh->DestroyComponent();

    SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
    SkeletalMesh->SetupAttachment(GetRootComponent());

    WeaponStatus = EWeaponStatus::EWS_Pickup;
}

void AWeapon::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
    Super::OnOverlapBegin(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

    AMainCharacter* MainCharacter = Cast<AMainCharacter>(OtherActor);
    if (MainCharacter && WeaponStatus == EWeaponStatus::EWS_Pickup)
    {
        MainCharacter->SetActiveOverlappingItem(this);
    }
}
	
void AWeapon::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
    Super::OnOverlapEnd(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex);

    AMainCharacter* MainCharacter = Cast<AMainCharacter>(OtherActor);
    if (MainCharacter && WeaponStatus == EWeaponStatus::EWS_Pickup)
    {
        MainCharacter->SetActiveOverlappingItem(nullptr);
    }
}

void AWeapon::Equip(AMainCharacter* Character)
{
    if (!Character) { return; }

    SkeletalMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
    SkeletalMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);

    SkeletalMesh->SetSimulatePhysics(false);

    const USkeletalMeshSocket* RightHandSocket = Character->GetMesh()->GetSocketByName(TEXT("RightHandSocket"));
    if (!RightHandSocket) { return; }

    RightHandSocket->AttachActor(this, Character->GetMesh());
    Character->SetWeaponEquipped(this);
    WeaponStatus = EWeaponStatus::EWS_Equipped;
    bRotating = false;

    if (EquipSound)
    {
        UGameplayStatics::PlaySound2D(this, EquipSound);
    }
}