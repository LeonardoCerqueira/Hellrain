// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Item.generated.h"

UCLASS()
class HELLRAIN_API AItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	/** Collider to interact with the item */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Items | Collision")
	class USphereComponent* ColliderVolume;

	/** Base mesh */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Items | Mesh")
	class UStaticMeshComponent* Mesh;

	/** Particles emitted when the item is idle */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Items | Particles")
	class UParticleSystemComponent* IdleParticles;

	/** Particles emitted on overlap with the item */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items | Particles")
	class UParticleSystem* OverlapParticles;

	/** Sound clip played on overlap with the item */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items | Sound")
	class USoundCue* SoundCue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items | ItemProperties")
	bool bRotating;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items | ItemProperties")
	bool bFloating;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items | ItemProperties")
	float RotationRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items | ItemProperties")
	float FloatSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items | ItemProperties")
	float FloatRange;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	
	UFUNCTION()
	virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

private:
	bool bGoingUp;

	FVector InitialLocation;
};
