// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MainCharacter.generated.h"

UENUM(BlueprintType)
enum class EMovementStatus : uint8
{
	EMS_Normal    UMETA(DisplayName = "Normal"),
	EMS_Sprinting UMETA(DisplayName = "Sprinting"),

	EMS_MAX       UMETA(DisplayName = "DefaultMAX")
};

UENUM(BlueprintType)
enum class EStaminaStatus : uint8
{
	ESS_Normal              UMETA(DisplayName = "Normal"),
	ESS_BelowMinimum        UMETA(DisplayName = "BelowMinimum"),
	ESS_Exhausted           UMETA(DisplayName = "Exhausted"),
	ESS_ExhaustedRecovering UMETA(DisplayName = "ExhaustedRecovering"),

	ESS_MAX                 UMETA(DisplayName = "DefaultMAX")
};

UCLASS()
class HELLRAIN_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMainCharacter();

	/** Camera boom positioning the camera behind the player */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow Camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	/** Base turn rates to scale turning functions for the camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	float NormalMaxSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	float SprintingMaxSpeed;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
	EMovementStatus MovementStatus;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	float StaminaDrainRate;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	float MinSprintStamina;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
	EStaminaStatus StaminaStatus;

	/** 
	 *
	 * Player Stats
	 * 
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Player Stats")
	float MaxHealth;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player Stats")
	float Health;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Player Stats")
	float MaxStamina;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player Stats")
	float Stamina;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player Stats")
	int32 Coins;

	/** 
	 * Equipment
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Items")
	class AWeapon* WeaponEquipped;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	bool bSprintKeyDown;
	bool bInteractKeyDown;

	/** Called during Tick, it manages stamina state, draining, recovering and movement state */
	void ManageStamina(float DeltaTime);

	class AItem* ActiveOverlappingItem;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	FORCEINLINE USpringArmComponent* GetCameraBoom() { return CameraBoom; }
	FORCEINLINE void SetCameraBoom(USpringArmComponent* InCameraBoom) { CameraBoom = InCameraBoom; }

	FORCEINLINE UCameraComponent* GetFollowCamera() { return FollowCamera; }
	FORCEINLINE void SetFollowCamera(UCameraComponent* InFollowCamera) { FollowCamera = InFollowCamera; }

	FORCEINLINE void SetStaminaStatus(EStaminaStatus Status) { StaminaStatus = Status; }

	void SetMovementStatus(EMovementStatus Status);

	/** Called for forwards/backwards input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** Called via input to turn at a given rate 
	 *  @param Rate This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/** Called via input to look up/down at a given rate 
	 *  @param Rate This is a normalized rate, i.e. 1.0 means 100% of desired look up/down rate
	*/
	void LookUpAtRate(float Rate);

	/** Called when the sprint key is pressed down */
	void StartSprint();

	/** Called when the sprint key is released */
	void EndSprint();

	/** Called when the interact key is pressed down */
	void StartInteraction();

	/** Called when the interact key is released */
	void EndInteraction();

	void IncrementHealth(float Amount);
	void DecrementHealth(float Amount);

	void IncrementStamina(float Amount);
	void DecrementStamina(float Amount);

	void IncrementCoins(int32 Amount);
	void DecrementCoins(int32 Amount);

	void Die();

	void SetWeaponEquipped(AWeapon* Weapon);

	FORCEINLINE void SetActiveOverlappingItem(AItem* Item) { ActiveOverlappingItem = Item; }
};
